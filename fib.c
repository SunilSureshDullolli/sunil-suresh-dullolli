/*
Author: Sunil Suresh Dullolli

Assumptions:
* fibonaci sequence up to 26 will be supported
* can be extended more then 26 but need to change data types to store values according (long long)
* Memotization is used to avoid recursive computaion of same values
* The instructions are followed as given in question
*/
#include <stdio.h>
#include <stdlib.h>
int f[50];
int fib(int n)
{

	if (f[n] != -1)
	{
		return f[n];
	}
	else
	{
		f[n] = fib(n - 1) + fib(n - 2);
		return f[n];
	}
}
int IsPrime(int k)
{
	if (k <= 1) return 1;
	if (k % 2 == 0 && k > 2) return 0;
	for (int i = 3; i < k / 2; i += 2)
	{
		if (k % i == 0)
			return 0;
	}
	return 1;

}
int main(void)
{
	f[0] = 0; 
	f[1] = 1;
	for (int i = 2; i < 51; i++)
	{
		f[i] = -1;
	}
	fib(25);
	for (int i = 0; f[i] != -1; i++)
	{

		if ((f[i] % 3 == 0) && (f[i] % 5 == 0)&& (f[i]!=0))
		{
			printf("%d - FizzBuzz\n",f[i]);
		}
		else if (f[i] % 3 == 0 && (f[i] != 0))
		{
			printf("%d - Buzz\n", f[i]);
		}
		else if (f[i] % 5 == 0 && (f[i] != 0))
		{
			printf("%d - Fizz\n", f[i]);
		}
		else if (IsPrime(f[i]) == 1)
		{
			printf("%d - BuzzFizz\n", f[i]);
		}
		else
		{
			printf("%d\n", f[i]);
		}

		
	}
}
